var express = require('express');
var app = express();
var cors = require('cors');

app.use(cors());

app.get('/', function (req, res) {
  res.send('Hello Worlds!\n');
});

app.use('/users',require('./routes/users'))

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
