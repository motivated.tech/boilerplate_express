var gulp   = require( 'gulp' ),
        server = require( 'gulp-develop-server' );

gulp.task('default', ['server:start'], function() {
});

// run server
 gulp.task( 'server:start', function() {

   server.listen( { path: './app.js' } );
   gulp.watch( [ './*.js', './routes/*.js' ], server.restart );

});
